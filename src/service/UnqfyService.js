import axios from 'axios';

const URL_SERVER = 'http://localhost';
const PORT = '8080'

class UnqfyService {

    getArtists() {
        return axios.get(`${URL_SERVER}:${PORT}/artists`);
    }

    getArtistById(id) {
        return axios.get(`${URL_SERVER}:${PORT}/artists/${id}`);
    }

}

export default new UnqfyService();